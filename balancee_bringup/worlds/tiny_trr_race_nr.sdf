<?xml version="1.0" ?>
<!--
  This demo imports the tiny Toulouse Robot Race
-->
<sdf version="1.6">
  <world name="tiny_trr_race_nr">

    <physics name="1ms" type="ignored">
        <max_step_size>0.001</max_step_size>
        <real_time_factor>1.0</real_time_factor>
    </physics>
    <plugin
        filename="gz-sim-physics-system"
        name="gz::sim::systems::Physics">
    </plugin>
    <plugin
        filename="gz-sim-user-commands-system"
        name="gz::sim::systems::UserCommands">
    </plugin>
    <plugin
        filename="gz-sim-scene-broadcaster-system"
        name="gz::sim::systems::SceneBroadcaster">
    </plugin>
    <plugin
        filename="gz-sim-imu-system"
        name="gz::sim::systems::Imu">
    </plugin>
    <plugin
        filename="gz-sim-sensors-system"
        name="gz::sim::systems::Sensors">
        <render_engine>ogre2</render_engine>
    </plugin>
    <plugin
        filename="gz-sim-contact-system"
        name="gz::sim::systems::Contact">
    </plugin>
        

    <light type="directional" name="sun">
      <cast_shadows>true</cast_shadows>
      <pose>0 0 10 0 0 0</pose>
      <diffuse>0.8 0.8 0.8 1</diffuse>
      <specular>0.2 0.2 0.2 1</specular>
      <attenuation>
        <range>1000</range>
        <constant>0.9</constant>
        <linear>0.01</linear>
        <quadratic>0.001</quadratic>
      </attenuation>
      <direction>-0.5 0.1 -0.9</direction>
    </light>

    <model name="ground_plane">
      <static>true</static>
      <link name="link">
        <collision name="collision_ground_plane">
          <geometry>
            <plane>
              <normal>0.0 0.0 1</normal>
              <size>100 100</size>
            </plane>
          </geometry>
        </collision>
        <visual name="visual">
          <geometry>
            <plane>
              <normal>0.0 0.0 1</normal>
              <size>100 100</size>
            </plane>
          </geometry>
          <material>
            <ambient>0.8 0.8 0.8 1</ambient>
            <diffuse>0.8 0.8 0.8 1</diffuse>
            <specular>0.8 0.8 0.8 1</specular>
          </material>
        </visual>
      </link>
    </model>

    <model name="top_camera">
      <static>true</static>
      <pose>0.1 0 3.5 0 1.57 1.57</pose>
      <link name="link">
        <pose>0.05 0.05 0.05 0 0 0</pose>
        <visual name="visual">
          <geometry>
            <box>
              <size>0.1 0.1 0.1</size>
            </box>
          </geometry>
        </visual>
        <sensor name="top_camera" type="camera">
          <camera>
            <horizontal_fov>2.047</horizontal_fov>
            <image>
              <width>640</width>
              <height>480</height>
            </image>
            <clip>
              <near>0.1</near>
              <far>100</far>
            </clip>
          </camera>
          <always_on>1</always_on>
          <update_rate>30</update_rate>
          <visualize>true</visualize>
          <topic>top_camera</topic>
        </sensor>
      </link>
    </model>

    
    <include>
        <static>true</static>
        <name>Tiny TRR Track</name>
        <pose>0 0 -0.06 0 0 0</pose>
        <uri> model://TRR Tiny Track</uri>
    </include>

    <include>
        <static>false</static>
        <name>TRR Samourai</name>
        <pose>-0.5 -1.35 0.32 0 0 3.14156</pose>
        <uri> model://TRR Samourai</uri>
    </include>

    <include>
        <static>false</static>
        <name>TRR Baby Walker</name>
        <pose>2.5 -1.65 0.03 0 0 3.14156</pose>
        <uri> model://TRR Baby Walker</uri>
    </include>

    <include>
        <static>false</static>
        <name>TRR Pink Panther</name>
        <pose>1.5 -1.95 0.03 0 0 3.14156</pose>
        <uri> model://TRR Pink Panther</uri>
    </include>

    <include>
        <static>true</static>
        <name>TRR Speed Bump</name>
        <pose>0.5 -1.65 0.005 0 0 3.14156</pose>
        <uri> model://TRR Speed Bump</uri>
    </include>

  </world>
</sdf>
