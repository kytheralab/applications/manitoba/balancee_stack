import os

from launch import LaunchDescription
from launch.substitutions import Command, FindExecutable, PathJoinSubstitution
from ament_index_python.packages import get_package_share_directory

from launch.conditions import IfCondition
from launch_ros.actions import Node
from launch.actions import ExecuteProcess, DeclareLaunchArgument,RegisterEventHandler, IncludeLaunchDescription
from launch_ros.substitutions import FindPackageShare
from launch.launch_description_sources import PythonLaunchDescriptionSource
from launch.substitutions import LaunchConfiguration
from launch.event_handlers import OnProcessExit
from launch_ros.substitutions import FindPackageShare



def generate_launch_description():

    pkg_balancee_control = get_package_share_directory('balancee_control')
    pkg_balancee_description = get_package_share_directory('balancee_description')

    # Launch configurations variables 
    use_joint_animation = LaunchConfiguration('use_joint_animation')

    robot_controllers = os.path.join(pkg_balancee_control, 'config/', 'balancee_controller.yaml')
    robot_description = os.path.join(pkg_balancee_description, 'urdf/', 'balancee.xacro')  


    declare_use_joint_animation = DeclareLaunchArgument(
        'use_joint_animation',
        default_value='false',
        description='Whether to start the Joint Animation Publisher')    

    control_node = Node(
        package="controller_manager",
        executable="ros2_control_node",
        parameters=[robot_description, robot_controllers],
        output="both",
    )

    joint_state_broadcaster_spawner = Node(
        package="controller_manager",
        executable="spawner",
        arguments=["joint_state_broadcaster", "--controller-manager", "/controller_manager"],
    )

    forward_position_controller_spawner = Node(
        package="controller_manager",
        executable="spawner",
        arguments=["forward_position_controller", "--controller-manager", "/controller_manager"],
    )

    diff_base_controller_spawner = Node(
        package="controller_manager",
        executable="spawner",
        arguments=["diff_drive_base_controller", "--controller-manager", "/controller_manager"],
    )


 

 
    # load_joint_state_broadcaster = ExecuteProcess(
	# 									cmd=['ros2', 'control', 'load_controller', '--set-state', 'active','joint_state_broadcaster'],
	# 									output='screen')
                                        
    # load_joint_trajectory_controller = ExecuteProcess( 
	# 									cmd=['ros2', 'control', 'load_controller', '--set-state', 'active', 'joint_trajectory_controller'], 
	# 									output='screen')

    # load_diff_drive_base_controller = ExecuteProcess(
    #                                     cmd=['ros2', 'control', 'load_controller', '--set-state', 'active',  'diff_drive_base_controller'],
    #                                     output='screen' )

    # Joint Animation Publisher Node (optional)
    trajectory_publisher_cmd = Node(
                condition=IfCondition(use_joint_animation),
                package='balancee_control',
                executable='joint_animation_node',
                name='joint_animation_node',        
                output='screen'
            )
    
    # Delay start of robot_controller after `joint_state_broadcaster`
    delay_forward_controller_spawner_after_joint_state_broadcaster_spawner = RegisterEventHandler(
        event_handler=OnProcessExit(
            target_action=joint_state_broadcaster_spawner,
            on_exit=[forward_position_controller_spawner],
        )
    ) 

    # Delay start of robot_controller after `joint_state_broadcaster`
    delay_diff_controller_spawner_after_joint_state_broadcaster_spawner = RegisterEventHandler(
        event_handler=OnProcessExit(
            target_action=joint_state_broadcaster_spawner,
            on_exit=[diff_base_controller_spawner],
        )
    ) 


    position_publisher = IncludeLaunchDescription(
        PythonLaunchDescriptionSource([
                PathJoinSubstitution([
                    FindPackageShare('balancee_control'),
                    'launch',
                    'test_forward_position_controller.launch.py'
                ])
            ]),
            launch_arguments={},
            condition=IfCondition(use_joint_animation)
    )

 

    ld = LaunchDescription()


    # ld.action(delay_robot_controller_spawner_after_joint_state_broadcaster_spawner)
    ld.add_action(declare_use_joint_animation)
    # ld.add_action(trajectory_publisher_cmd)
    ld.add_action(control_node)
    ld.add_action(joint_state_broadcaster_spawner)
    ld.add_action(delay_diff_controller_spawner_after_joint_state_broadcaster_spawner)
    # ld.add_action(delay_forward_controller_spawner_after_joint_state_broadcaster_spawner)
    # ld.add_action(position_publisher)

    
    # ld.add_action(load_joint_trajectory_controller)
    # ld.add_action(load_diff_drive_base_controller)
    # trajectory_publisher_node,
  

    return ld

